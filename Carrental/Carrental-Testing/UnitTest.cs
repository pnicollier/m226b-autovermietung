﻿using Carrental;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Carrental_Testing
{
    [TestClass]
    public class UnitTest
    {
        private Tuple<Location, Client> GetVariables()
        {
            // Get variables from test file WHERE ONLY ONE CONTRACT HAS BEEN MADE
            IFormatter formatter = new BinaryFormatter();

            Stream outputStreamLocation = new FileStream(@"../../../ObjectsData/TestFiles/Location.txt", FileMode.Open, FileAccess.Read);
            Stream outputStreamClient = new FileStream(@"../../../ObjectsData/TestFiles/Client.txt", FileMode.Open, FileAccess.Read);

            Location location = (Location)formatter.Deserialize(outputStreamLocation);
            Client client = (Client)formatter.Deserialize(outputStreamClient);
            outputStreamLocation.Close();
            outputStreamClient.Close();
            return Tuple.Create(location, client);
        }

        [TestMethod]
        public void UnitTestLocationMethodAreAvailableTrucksHere()
        {
            Truck truck = new Truck(3, 120, "Merceydes", "Big 'Ol Truck", new DateTime(1989, 10, 22), EngineType.Intake,
                150, 3, 8, "Truck Driva", false, 0, 0, true, "Glass, Nuclear Bomb");
            Location location = new Location();
            location.Vehicles.Add(truck);
            Assert.IsTrue(location.AreAvailableTrucksHere());
            Assert.IsFalse(location.AreAvailableCarsHere());
        }

        [TestMethod]
        public void UnitTestLocationMethodAreAvailableCarsHere()
        {
            Location location = new Location();
            Assert.IsFalse(location.AreAvailableCarsHere());
        }

        [TestMethod]
        public void UnitTestLocationMethodGetYearSales()
        {
            Location location = GetVariables().Item1;
            // Because no contracts has been signed
            Assert.IsFalse(location.GetYearSales() == 0);
        }

        [TestMethod]
        public void UnitTestEmployeeAndClientMethodGetLastName()
        {
            Client client = GetVariables().Item2;
            Assert.IsNotNull(client.GetLastName());
        }

        [TestMethod]
        public void UnitTestEmployeeAndClientMethodContractVehicleTypeEqual()
        {
            Location location = GetVariables().Item1;
            Client client = GetVariables().Item2;
            Assert.IsInstanceOfType(client.SignedContracts.First().Vehicle, location.ContractsMade.First().Vehicle.GetType());
        }

        [TestMethod]
        public void UnitTestEmployeeAndClientMethodGetRentedVehicle()
        {
            Client client = GetVariables().Item2;
            Assert.IsNotNull(client.GetRentedVehicle(1));
        }

        [TestMethod]
        public void UnitTestVehicleIsRentableMethod()
        {
            Location location = GetVariables().Item1;
            Assert.IsTrue(location.Vehicles[4].IsRentable());
        }

        [TestMethod]
        public void UnitTestPaymentIsPaid()
        {
            Location location = GetVariables().Item1;
            Assert.IsTrue(location.ContractsMade.First().Payment.IsPaid);
        }
    }
}