﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Carrental
{
    [Serializable]
    public class Truck : Vehicle
    {
        public Truck(int id, double pricePerDay, string manufacturer, string model, DateTime creationDate, EngineType engineType, double maxSpeed, int seatCount, int wheelCount, string owner, bool hasTrailer, double trailerWeight, int trailerWheelCount, bool hasDangerousOrFragileGoods, string goods)
        {
            Id = id;
            PricePerDay = pricePerDay;
            Manufacturer = manufacturer;
            Model = model;
            CreationDate = creationDate;
            EngineType = engineType;
            MaxSpeed = maxSpeed;
            SeatCount = seatCount;
            WheelCount = wheelCount;
            Owner = owner;
            HasTrailer = hasTrailer;
            TrailerWeight = trailerWeight;
            TrailerWheelCount = trailerWheelCount;
            HasDangerousOrFragileGoods = hasDangerousOrFragileGoods;
            Goods = goods;
        }

        public bool HasTrailer { get; set; }
        public double TrailerWeight { get; set; }
        public int TrailerWheelCount { get; set; }
        public bool HasDangerousOrFragileGoods { get; set; }
        public string Goods { get; set; }

        public new void PrintData()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("--------------------------------Truck-------------------------------------");
            base.PrintData();
            Console.WriteLine($"|Has Trailer: {HasTrailer}" + (HasTrailer ? $", Trailer Weight: {TrailerWeight}, Trailer Wheel Count: {TrailerWheelCount}" : "") + $", Has Dangerous Goods: {HasDangerousOrFragileGoods}, Goods: {Goods}");
            Console.WriteLine("--------------------------------------------------------------------------");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
