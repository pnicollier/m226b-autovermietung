﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Carrental
{
    [Serializable]
    public class Client : Person
    {

        public Client(int id, string firstName, string lastName, DateTime birthday, string address, DateTime entryDate, List<Vehicle> rentedVehicles, List<Contract> signedContracts, double money)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Birthday = birthday;
            Address = address;
            EntryDate = entryDate;
            SignedContracts = signedContracts;
            RentedVehicles = rentedVehicles;
            Money = money;
        }
        public int Id { get; set; }
        public List<Vehicle> RentedVehicles { get; set; } = new List<Vehicle>();
        public List<Contract> SignedContracts { get; set; } = new List<Carrental.Contract>();
        public double Money { get; set; }

        public new void PrintData()
        {
            Console.WriteLine("\n----------------------------------Client-----------------------------------");
            Console.WriteLine($"Id: {Id}, Rented Vehicles: {RentedVehicles.Count}, Signed Contracts: {SignedContracts.Count}, Money: {Money}");
            base.PrintData();
            Console.WriteLine("-----------------------------------------------------------------------------");
        }

        public void PrintRentedVehicles()
        {
            if (RentedVehicles.Count == 0)
            {
                Console.WriteLine("You have no rented vehicles.");
            }
            else
            {
                foreach (Vehicle rentedVehicle in RentedVehicles)
                {
                    if (rentedVehicle.GetType() == typeof(Car))
                    {
                        ((Car)rentedVehicle).PrintData();
                    }
                    else if (rentedVehicle.GetType() == typeof(Truck))
                    {
                        ((Truck)rentedVehicle).PrintData();
                    }
                }
            }
        }

        public string GetLastName()
        {
            return LastName;
        }

        public Vehicle GetRentedVehicle(int id)
        {
            Vehicle rentedVehicle = RentedVehicles.Single(x => x.Id == id);
            return rentedVehicle;
        }

        public void Pay(Payment payment)
        {
            if (payment != null && Money > payment.Amount)
            {
                Money -= payment.Amount;
                payment.IsPaid = true;
            }
        }
    }
}