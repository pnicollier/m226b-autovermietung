﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrental
{
    [Serializable]
    public class Employee : Person
    {
        public Employee(int id, string firstName, string lastName, DateTime birthday, string address, DateTime entryDate, Profession profession, double salary, bool isAvailable, List<Contract> signedContracts)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Birthday = birthday;
            Address = address;
            EntryDate = entryDate;
            Profession = profession;
            Salary = salary;
            IsAvailable = isAvailable;
            SignedContracts = signedContracts;
        }
        public int Id { get; set; }
        public Profession Profession { get; set; }
        public double Salary { get; set; }
        public bool IsAvailable { get; set; }
        public List<Contract> SignedContracts { get; set; } = new List<Contract>();

        public new void PrintData()
        {
            Console.WriteLine("\n---------------------------------Employee------------------------------------");
            Console.WriteLine($"Id: {Id}, Signed Contracts: {SignedContracts.Count}");
            base.PrintData();
            Console.WriteLine("-------------------------------------------------------------------------------");
        }

        public void Say(string text)
        {
            Console.WriteLine(text);
        }

        public string GetLastName()
        {
            return LastName;
        }
    }

    public enum Profession
    {
        InOffice,
        Renter
    }
}
