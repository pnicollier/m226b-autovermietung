﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrental
{
    [Serializable]
    public class Car : Vehicle
    {
        public Car(int id, double pricePerDay, string manufacturer, string model, DateTime creationDate, EngineType engineType, double maxSpeed, int seatCount, int wheelCount, string owner)
        {
            Id = id;
            PricePerDay = pricePerDay;
            Manufacturer = manufacturer;
            Model = model;
            CreationDate = creationDate;
            EngineType = engineType;
            MaxSpeed = maxSpeed;
            SeatCount = seatCount;
            WheelCount = wheelCount;
            Owner = owner;
        }
        public new void PrintData()
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("---------------------------------Car--------------------------------------");
            base.PrintData();
            Console.WriteLine("--------------------------------------------------------------------------");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
