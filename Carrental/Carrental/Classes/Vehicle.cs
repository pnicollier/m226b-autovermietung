﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Carrental
{
    [Serializable]
    public abstract class Vehicle
    {
        public int Id { get; set; }
        public double PricePerDay { get; set; }
        public DateTime DateTillNotAvailable { get; set; }
        protected string Manufacturer { get; set; }
        protected string Model { get; set; }
        protected DateTime CreationDate { get; set; }
        protected EngineType EngineType { get; set; }
        protected double MaxSpeed { get; set; }
        protected int SeatCount { get; set; }
        protected int WheelCount { get; set; }
        protected string Owner { get; set; }

        public bool IsRentable()
        {
            return DateTime.Today > DateTillNotAvailable.Date;
        }

        protected int DaysAvailable()
        {
            return (DateTillNotAvailable - DateTime.Today).Days;
        }

        protected void PrintData()
        {
            Console.WriteLine($"|Id: {Id}, Max Speed: {MaxSpeed}, Is Available: {IsRentable()}" + (IsRentable() ? $", Days Available: {DaysAvailable()}" : "") + $", Owner: {Owner}" +
                              $"\n|Manufacturer: {Manufacturer}, Model: {Model}, Creation Date: {CreationDate.Date.ToString("d")}, Engine Type: {EngineType.ToString()}, Seat Count: {SeatCount}, WheelCount: {WheelCount}, Price Per Day: {PricePerDay}");
        }

        public void Drive()
        {
            Console.WriteLine($"Niuuuu! at Speed of {MaxSpeed}m/s");
        }
    }

    public enum EngineType
    {
        Intake,
        Compression,
        Combustion,
        Exhaust
    }
}