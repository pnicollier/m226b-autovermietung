﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrental
{
    [Serializable]
    public class Payment
    {
        public Payment(double amount, PaymentMethod paymentMethod, DateTime paymentDate)
        {
            Amount = amount;
            PaymentMethod = paymentMethod;
            PaymentDate = paymentDate;
        }

        public double Amount { get; set; }
        public PaymentMethod PaymentMethod { get; set; }
        public DateTime PaymentDate { get; set; }
        public bool IsPaid { get; set; }
    }

    public enum PaymentMethod
    {
        CreditOrDebitCard,
        Invoice,
        Cash
    }
}
