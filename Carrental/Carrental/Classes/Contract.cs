﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Carrental
{
    [Serializable]
    public class Contract
    {
        public Contract(Client client, Vehicle vehicle, Employee employee, DateTime creationDate, DateTime expireDate, Payment payment)
        {
            Client = client;
            Vehicle = vehicle;
            Employee = employee;
            CreationDate = creationDate;
            ExpireDate = expireDate;
            Payment = payment;
        }
        public Client Client { get; set; }
        public Vehicle Vehicle { get; set; }
        public Employee Employee { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ExpireDate { get; set; }
        public Payment Payment { get; set; }
    }
}
