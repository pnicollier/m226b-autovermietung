﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Carrental
{
    [Serializable]
    public class Location
  {
    public string Address { get; set; }
    public List<Vehicle> Vehicles { get; set; } = new List<Vehicle>();
    public List<Employee> CurrentEmployees { get; set; } = new List<Employee>();
    public List<Contract> ContractsMade { get; set; } = new List<Contract>();

    public void PrintAvailableRenterEmployees()
    {
      foreach (Employee employee in CurrentEmployees.Where(x => x.IsAvailable == true && x.Profession == Profession.Renter))
      {
        employee.PrintData();
      }
    }

    public void PrintVehiclesAtLocation()
    {
      foreach (Vehicle vehicle in Vehicles)
      {
        if (vehicle.GetType() == typeof(Car))
        {
          Car car = (Car) vehicle;
          car.PrintData();
        }
        else if (vehicle.GetType() == typeof(Truck))
        {
          Truck truck = (Truck)vehicle;
          truck.PrintData();
        }
      }
    }

    public double GetYearSales()
    {
      double sales = 0;
      foreach (Contract contract in ContractsMade)
      {
          sales += contract.Payment.Amount;
      }

      return sales;
    }

    public Employee GetAvailableEmployee(int id)
    {
      return CurrentEmployees.Find(x => x.Id == id && x.IsAvailable && x.Profession == Profession.Renter);
    }

    public void PrintAvailableCars()
    {
      if (AreAvailableCarsHere())
      {
        foreach (Vehicle vehicle in Vehicles)
        {
          if (vehicle.GetType() == typeof(Car) && vehicle.IsRentable())
            ((Car)vehicle).PrintData();
        }
      }
      else
      {
        Console.WriteLine("There are no available Cars.");
      }
    }

    public void PrintAvailableTrucks()
    {
      if(AreAvailableTrucksHere())
      { 
        foreach (Vehicle vehicle in Vehicles)
        {
          if (vehicle.GetType() == typeof(Truck) && vehicle.IsRentable())
            ((Truck)vehicle).PrintData();
        }
      }
      else
      {
        Console.WriteLine("There are no available Trucks.");
      }
    } 

    public Truck GetAvailableTruck(int id)
    {
        Truck aboutToBeRentedTruck;
      aboutToBeRentedTruck = (Truck)Vehicles.Find(x=> x.Id == id && x.IsRentable() && x.GetType() == typeof(Truck));
      return aboutToBeRentedTruck;
    }

    public Car GetAvailableCar(int id)
    {
        Car aboutToBeRentedCar;
      aboutToBeRentedCar = (Car)Vehicles.Find(x => x.Id == id && x.IsRentable() && x.GetType() == typeof(Car));
      return aboutToBeRentedCar;
    }

    public bool AreAvailableCarsHere()
    {
      if (Vehicles.Count(x=> x.IsRentable() && x.GetType() == typeof(Car)) != 0)
      {
        return true;
      }
      return false;
    }

    public bool AreAvailableTrucksHere()
    {
      if (Vehicles.Count(x => x.IsRentable() && x.GetType() == typeof(Truck)) != 0)
      {
        return true;
      }
      return false;
    }

    public string RentVehicle(Client client, Vehicle vehicle, Employee employee, int duration, Contract contract)
    {
        string result;
        if (contract.Payment.IsPaid)
        {
            Vehicles.Find(x => x.Id == vehicle.Id).DateTillNotAvailable = DateTime.Now.AddDays(duration);
            ContractsMade.Add(contract);
            client.RentedVehicles.Add(vehicle);
            client.SignedContracts.Add(contract);
            employee.SignedContracts.Add(contract);
            result = "The vehicle is yours!";
            
        }
        else
        {
            result = "The vehicle hasn't been paid yet. You maybe don't have enough money!";
        }

        return result;
    }
  }
}
