﻿using System;

namespace Carrental
{
    [Serializable]
    public abstract class Person
    {
        protected string FirstName { get; set; }
        protected string LastName { get; set; }
        protected DateTime Birthday { get; set; }
        protected string Address { get; set; }
        protected DateTime EntryDate { get; set; }

        protected void PrintData()
        {
            Console.WriteLine($"Firstname: {FirstName}, Lastname: {LastName}, Age: {DateTime.Now.Year - Birthday.Year}, Member since: {EntryDate.Date.ToString("d")}");
        }
    }
}
