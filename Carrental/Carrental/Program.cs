﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mime;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Carrental
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Deserialize Objects from files(Get old Data)
            IFormatter formatter = new BinaryFormatter();

            Stream outputStreamLocation = new FileStream(@"../../../ObjectsData/Location.txt", FileMode.Open, FileAccess.Read);
            Stream outputStreamClient = new FileStream(@"../../../ObjectsData/Client.txt", FileMode.Open, FileAccess.Read);

            Location location = (Location)formatter.Deserialize(outputStreamLocation);
            Client client = (Client)formatter.Deserialize(outputStreamClient);
            outputStreamLocation.Close();
            outputStreamClient.Close();
            #endregion

            #region Main_Program
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Hello to the Carrental By Philipp Nicollier!");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Press Enter to enter the Location.");
            Console.ReadLine();

            bool exit = false;
            while (!exit)
            {
                switch (PrintMenuInLocationAndGetOption())
                {
                    case "1":
                        Console.Clear();
                        Console.WriteLine("These Vehicles are at Location: " + location.Address);
                        location.PrintVehiclesAtLocation();
                        Console.WriteLine("Press Enter to go back");
                        Console.ReadLine();
                        break;
                    case "2":
                        Console.Clear();
                        Console.WriteLine("Your Data:");
                        client.PrintData();
                        Console.WriteLine("Press Enter to go back");
                        Console.ReadLine();
                        break;
                    case "3":
                        RentVehicle();
                        break;
                    case "4":
                        Console.Clear();
                        Console.WriteLine("These are your rented vehicles:");
                        Console.WriteLine("--------------------------------------------------------------------------------------------------------------------------------------------------------------");
                        client.PrintRentedVehicles();
                        Console.WriteLine("--------------------------------------------------------------------------------------------------------------------------------------------------------------");
                        Console.WriteLine("Press Enter to go back");
                        Console.ReadLine();
                        break;
                    case "5":
                        if (client.RentedVehicles.Count == 0)
                        {
                            Console.Clear();
                            Console.WriteLine("You have no rented vehicles to drive.");
                            Console.WriteLine("Press Enter to go back");
                            Console.ReadLine();
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("Which of your rented vehicles would you like to drive?:");
                            Console.WriteLine("--------------------------------------------------------------------------------------------------------------------------------------------------------------");
                            client.PrintRentedVehicles();
                            Console.WriteLine("--------------------------------------------------------------------------------------------------------------------------------------------------------------");
                            Console.WriteLine("Type the Id of the vehicle to drive it");
                            Vehicle wantToDriveVehicle = client.GetRentedVehicle(int.Parse(Console.ReadLine()));
                            if (wantToDriveVehicle != null)
                            {
                                wantToDriveVehicle.Drive();
                                while (true)
                                {
                                    Console.WriteLine("Do you still want to drive your vehicle? (Enter/\"exit\")");
                                    string input = Console.ReadLine();
                                    if (input == "")
                                    {
                                        wantToDriveVehicle.Drive();
                                    }
                                    else if (input.ToLower() == "exit")
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                    case "6":
                        Console.Clear();
                        Console.WriteLine($"Year Sales: {location.GetYearSales()}");
                        Console.WriteLine("Press Enter to go back");
                        Console.ReadLine();
                        break;
                    case "7":
                        exit = true;
                        break;
                }
            }
            #endregion

            #region Functions
            string PrintMenuInLocationAndGetOption()
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Location: {location.Address}");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("1. Look at vehicles");
                Console.WriteLine("2. Look at ur data");
                Console.WriteLine("3. Rent a vehicle");
                Console.WriteLine("4. Look at your rented vehicles");
                Console.WriteLine("5. Drive one of your rented vehicles");
                Console.WriteLine("6. Get Year Sales");
                Console.WriteLine("7. Exit");
                return Console.ReadLine();
            }

            void RentVehicle()
            {
                // Picking employee
                Console.Clear();
                Console.WriteLine("Pick an employee:");
                location.PrintAvailableRenterEmployees();
                Employee employee;
                bool employeeFound = false;
                while (!employeeFound)
                {
                    Console.WriteLine("Type the Id of the employee to talk with him");
                    if (int.TryParse(Console.ReadLine(), out int id1))
                    {
                        employee = location.GetAvailableEmployee(id1);
                        if (employee != null)
                        {
                            employeeFound = true;
                            // Picking type of vehicle
                            employee.Say($"Hello Mr.{client.GetLastName()}! I'm {employee.GetLastName()}.");
                            bool choosingVehicle = true;
                            while (choosingVehicle)
                            {
                                employee.Say("Which type of vehicle would you like to rent?");
                                employee.Say("1. Car");
                                employee.Say("2. Truck");
                                employee.Say("3. Go Back");
                                Car car;
                                Truck truck;
                                switch (Console.ReadLine())
                                {
                                    case "1":
                                        if (location.AreAvailableCarsHere())
                                        {
                                            employee.Say("These are the available Cars:");
                                            location.PrintAvailableCars();
                                            employee.Say("Type the id of the car you would like to rent");
                                            while (true)
                                            {
                                                if (int.TryParse(Console.ReadLine(), out int id))
                                                {
                                                    car = location.GetAvailableCar(id);
                                                    if (car != null && car.IsRentable())
                                                    {
                                                        int days = 0;
                                                        employee.Say("For how long? (In Days)");
                                                        bool choosingTime = true;
                                                        while (choosingTime)
                                                        {
                                                            if (int.TryParse(Console.ReadLine(), out days) && days > 0 && days < 730)
                                                            {
                                                                choosingTime = false;
                                                            }
                                                            else
                                                            {
                                                                Console.WriteLine("For how long? (In days) It should be at least more than 0 days and less than 2 years!");
                                                            }
                                                        }

                                                        Contract contract = SignContractAndPay(car, employee, days);
                                                        if (contract != null)
                                                        {
                                                            Console.WriteLine(location.RentVehicle(client, car, employee, days, contract));
                                                            Console.WriteLine("Press enter to go back");
                                                            choosingVehicle = false;
                                                            Console.ReadLine();
                                                        }
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine("No available car with that id");
                                                    }
                                                }
                                                else
                                                {
                                                    employee.Say("No available car with that id!");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            employee.Say($"I'm sorry Mr.{client.GetLastName()}, but there are no available rentable cars...");
                                        }
                                        break;
                                    case "2":
                                        if (location.AreAvailableTrucksHere())
                                        {
                                            employee.Say("These are the available trucks:");
                                            location.PrintAvailableTrucks();
                                            while (true)
                                            {
                                                employee.Say("Type the id of the truck you would like to rent");
                                                if (int.TryParse(Console.ReadLine(), out int id))
                                                {
                                                    truck = location.GetAvailableTruck(id);
                                                    if (truck != null && truck.IsRentable())
                                                    {
                                                        int days = 0;
                                                        employee.Say("For how long? (In Days)");
                                                        bool choosingTime = true;
                                                        while (choosingTime)
                                                        {
                                                            if (int.TryParse(Console.ReadLine(), out days) && days > 0 && days < 730)
                                                            {
                                                                choosingTime = false;
                                                            }
                                                            else
                                                            {
                                                                Console.WriteLine("For how long? (In days) It should be at least more than 0 days and less than 2 years!");
                                                            }
                                                        }

                                                        Contract contract = SignContractAndPay(truck, employee, days);
                                                        if (contract != null)
                                                        {
                                                            Console.WriteLine(location.RentVehicle(client, truck, employee, days, contract));
                                                            Console.WriteLine("Press enter to go back");
                                                            choosingVehicle = false;
                                                            Console.ReadLine();
                                                        }
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        Console.WriteLine("No available truck with that id");
                                                    }
                                                }
                                                else
                                                {
                                                    employee.Say("No available truck with that id!");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            employee.Say($"I'm sorry Mr.{client.GetLastName()}, but there are no available rentable trucks...");
                                            employee.Say("Would you like to look at other vehicles?");
                                        }
                                        break;
                                    case "3":
                                        choosingVehicle = false;
                                        break;
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine("There is no employee with that id");
                        }
                    }
                    else
                    {
                        Console.WriteLine("The input was in wrong format! Please enter an id from an employee");
                    }
                }
            }

            Contract SignContractAndPay(Vehicle vehicle, Employee employee, int duration)
            {
                Contract contract = new Contract(client, vehicle, employee, DateTime.Now,
                    DateTime.Now.AddDays(duration),
                    new Payment((vehicle.PricePerDay * duration), PaymentMethod.CreditOrDebitCard, DateTime.Today));
                bool choosingPaymentMethod = true;
                while (choosingPaymentMethod)
                {
                    int i = 0;
                    employee.Say("How would you like to pay?");
                    foreach (PaymentMethod paymentMethod in (PaymentMethod[])Enum.GetValues(typeof(PaymentMethod)))
                    {
                        employee.Say(i + paymentMethod.ToString());
                        i++;
                        if (i == 3)
                            break;
                    }

                    switch (Console.ReadLine().ToLower())
                    {
                        case "0":
                            contract.Payment.PaymentMethod = PaymentMethod.CreditOrDebitCard;
                            choosingPaymentMethod = false;
                            break;
                        case "1":
                            contract.Payment.PaymentMethod = PaymentMethod.Invoice;
                            choosingPaymentMethod = false;
                            break;
                        case "2":
                            contract.Payment.PaymentMethod = PaymentMethod.Cash;
                            choosingPaymentMethod = false;
                            break;
                    }
                }
                employee.Say($"Would you really like to rent this vehicle for a total of {vehicle.PricePerDay * duration} for {duration} days? (Y/N)");
                while (true)
                {
                    switch (Console.ReadLine().ToLower())
                    {
                        case "y":
                            client.Pay(contract.Payment);
                            return contract;
                        case "n":
                            return null;
                        default:
                            Console.WriteLine("Not an option");
                            break;
                    }
                }
            }
            #endregion

            #region Serialize Objects back into  files(Save new Data)
            Stream inputStreamLocation = new FileStream(@"../../../ObjectsData/Location.txt", FileMode.Create, FileAccess.Write);
            Stream inputStreamClient = new FileStream(@"../../../ObjectsData/Client.txt", FileMode.Create, FileAccess.Write);

            formatter.Serialize(inputStreamLocation, location);
            formatter.Serialize(inputStreamClient, client);
            inputStreamLocation.Close();
            inputStreamClient.Close();
            #endregion
        }
    }
}
